<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>
        <ol class="breadcrumb">

            <li class=""><a href="index.html">Home</a></li>
            <li class="active"><a href="index.html">Dashboard</a></li>

        </ol>
        <div class="page-heading">
            <h1>New Payment</h1>

            <div class="options">

            </div>
        </div>
        <div class="container-fluid">


            <div class="col-sm-6 col-md-offset-3">
                <?php $this->load->view('includes/notification'); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>New Payment</h2>
                    </div>
                    <form action="<?php echo base_url('payment/pay'); ?>" id="validate-form" class="form-horizontal" data-parsley-validate method="post" accept-charset="utf-8">

                    <div class="panel-body">
                        <div class="form-group mb-md">

                            <div class="col-xs-8 col-xs-offset-2">
                                <b>Amount</b>
                                <input type="text" class="form-control" name="amount" id="amount"
                                       placeholder="" value="" required>

                            </div>
                        </div>
                         <div class="form-group mb-md">

                            <div class="col-xs-8 col-xs-offset-2">
                                <b>Meter Number</b>
                                <input type="text" class="form-control" name="meter_no" id="meter_no"
                                       placeholder="" value="<?=$this->aauth->get_user()->meter_no?>" readonly required>

                            </div>
                        </div>
                        <div class="form-group mb-md">
                            <div class="col-xs-8 col-xs-offset-2">
                                <b>Payment Description</b>
                                <textarea  class="form-control" placeholder="Payment Description"  required name="description">
                                </textarea>
                            </div>
                        </div>

                    </div>
                    <div class="panel-footer">
                        <div class="clearfix">

                            <button type="submit" class="btn btn-primary btn-raised pull-right">PAY</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>
