<?php


class Merchant_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();

	}
	public function save($data)
	{
		if($this->db->insert('merchant_info', $data)){
            $lastId = $this->db->insert_id();
            return true;  
        }
        
        return false;
		

	}
    
    public function update($data,$user_id)
	{
        $this->db->where('merchant_id', $user_id);
        if($this->db->update('merchant_info', $data)){
            return true;
        }
		
        return false;
	
	}
    
    
	public function get($id)
	{
		$this->db->select('*')
            ->from('merchant_info')
            ->join('aauth_users', 'aauth_users.id = merchant_info.merchant_id')
            ->where('merchant_info.merchant_id',$id);
        
		$query = $this->db->get();
		return $query->row();
	}
    
    
    
    public function get_all($limit = FALSE,$offset = FALSE)
	{    
        $this->db->select('*')
                ->from('merchant_info')
                ->join('aauth_users', 'aauth_users.id = merchant_info.merchant_id');
        
        if($limit){
            $this->db->limit($limit);
            if($offset)
                $this->db->limit($limit,$offset);
        }         
        $this->db->order_by('date_created', 'DESC');

        $query = $this->db->get();

        return $query->result();
	}
    
}