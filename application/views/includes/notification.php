<?php if($this->session->flashdata('errors')): ?>
<div style="margin-top:10px;margin-bottom:10px;">
    <div class="alert alert-danger">
        <?php echo ($this->session->flashdata('errors')); ?>
    </div>
</div>
<?php endif; ?>
<?php if($this->session->flashdata('success')): ?>
<div style="margin-top:10px;margin-bottom:10px;">
    <div class="alert alert-success">
        <?php echo ($this->session->flashdata('success')); ?>
    </div>
</div>
<?php endif; ?>