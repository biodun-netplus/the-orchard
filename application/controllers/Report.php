<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Coupon_model');
        $this->load->model('Product_model');
        $this->load->model('User_model');
        $this->load->library('form_validation');

        if (!$this->aauth->is_loggedin())
            return redirect('login', 'refresh');
        if ($this->aauth->is_member('Merchant') || $this->aauth->is_member('Public')) {
            show_error('Access Denied');
        }

    }

    public function index()
    {
        $data = array(
            'title' => 'Reports'
        );

        $data['users'] = $this->aauth->list_users('Public');
        $this->template->load('default', 'report/index', $data);
    }

    public function export_payment_report()
    {
        if (!$this->input->post()) {
            redirect('report');
        }
        $this->form_validation->set_rules('payment_report_start_date', 'Payment Report Start Date', 'trim|required');
        $this->form_validation->set_rules('payment_report_end_date', 'Payment Report End Date', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('errors', validation_errors());
            redirect('report');
        }
        $start_date = $this->input->post('payment_report_start_date', true);
        $end_date = $this->input->post('payment_report_end_date', true);

        if ($this->aauth->is_member('Admin')) {
            $payments = $this->db->query("
            SELECT cart.order_id,cart.quantity,payments.*,product.product_name,product.product_price,product.property_type,product.product_type,product.partner_type,
            cart.*,aauth_users.full_name,aauth_users.house_address,aauth_users.type_of_property,aauth_users.cug_no
            FROM payments Inner Join cart On payments.payment_id = cart.order_id INNER JOIN product on cart.product_id = product.id INNER JOIN aauth_users on payments.user_id = aauth_users.id AND payments.date_created BETWEEN '" . $start_date . "' and '" . $end_date . "'")->result();

        }

        if ($this->aauth->is_member('Report') || $this->aauth->is_member('Finance')) {
            $payments = $this->db->query("
            SELECT cart.order_id,cart.quantity,payments.*,product.product_name,product.product_price,product.property_type,product.product_type,product.partner_type,
            cart.*,aauth_users.full_name,aauth_users.house_address,aauth_users.type_of_property,aauth_users.cug_no
            FROM payments Inner Join cart On payments.payment_id = cart.order_id INNER JOIN product on cart.product_id = product.id INNER JOIN aauth_users on payments.user_id = aauth_users.id AND payments.payment_type = 'Card'  AND payments.date_created BETWEEN '" . $start_date . "' and '" . $end_date . "'")->result();

        }

        $csvdata[] = array(
            'User ID',
            'Order ID',
            'Full Name',
            'Meter No',
            'Property',
            'House Address',
            'CUG',
            'Bill Name',
            'Bill Type',
            'Bill Amount',
            'Amount Paid',
            'Quantity',
            'Power Token',
            'Power Token Desc',
            'Power Token Unit',
            'Payment Method',
            'Payment Status',
            'Coupon ID',
            'Date Paid',
        );
        foreach ($payments as $payment) {
            $cuopon = $this->Coupon_model->get($payment->coupon_id);
            $csvdata[] = array(
                $payment->user_id,
                $payment->order_id,
                $payment->full_name,
                $payment->meter_no,
                $payment->type_of_property,
                $payment->house_address,
                $payment->cug_no,
                $payment->product_name,
                $payment->product_type,
                $payment->amount,
                $payment->amount_paid,
                $payment->quantity,
                $payment->token_no,
                $payment->token_desc,
                $payment->token_amount,
                $payment->payment_type,
                $payment->status,
                $cuopon ? $cuopon->code : '',
                $payment->date_created
            );
        }
//var_dump($orders);
//exit;

        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"payment-report-export-" . date('Y-m-d') . ".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");

        $handle = fopen('php://output', 'w');
//fputcsv($handle, array('id','order id',''));
        foreach ($csvdata as $order) {
            fputcsv($handle, $order);
        }
        fclose($handle);
        exit;
    }
    public function export_user_report()
    {
        if (!$this->input->post()) {
            redirect('report');
        }
        $this->form_validation->set_rules('user', 'Payment Report Start Date', 'trim|required');
        $this->form_validation->set_rules('user_report_start_date', 'Payment Report Start Date', 'trim|required');
        $this->form_validation->set_rules('user_report_end_date', 'Payment Report End Date', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('errors', validation_errors());
            redirect('report');
        }
        $user_id = $this->input->post('user', true);
        $user = $this->aauth->get_user($user_id);
        $start_date = $this->input->post('user_report_start_date', true);
        $end_date = $this->input->post('user_report_end_date', true);
        $csvdata[] = ['User ID',$user_id,'','','','','','','','','','','',''];
        $csvdata[] = ['Full Name',$user->full_name,'','','','','','','','','','','',''];
        $csvdata[] = ['Meter No',$user->meter_no,'','','','','','','','','','','',''];
        $csvdata[] = ['Property',$user->type_of_property,'','','','','','','','','','','',''];
        $csvdata[] = ['House Address',$user->house_address,'','','','','','','','','','','',''];
        $csvdata[] = ['CUG',$user->cug_no,'','','','','','','','','','','',''];
        $csvdata[] = ['','','','','','','','','','','','','',''];
        $csvdata[] = ['','','','','','','','','','','','','',''];
        $csvdata[] = ['','','','','','','','','','','','','',''];

        $payments = $this->db->query("
            SELECT cart.order_id,cart.quantity,payments.*,product.product_name,product.product_price,product.property_type,product.product_type,product.partner_type,
            cart.*
            FROM payments Inner Join cart On payments.payment_id = cart.order_id INNER JOIN product on cart.product_id = product.id AND payments.user_id = '".$user_id."' AND payments.date_created BETWEEN '" . $start_date . "' and '" . $end_date . "'")->result();

        $csvdata[] = array(
            'Order ID',
            'Bill Name',
            'Bill Type',
            'Bill Amount',
            'Amount Paid',
            'Quantity',
            'Power Token',
            'Power Token Desc',
            'Power Token Unit',
            'Payment Method',
            'Payment Status',
            'Coupon ID',
            'Date Paid',
        );
        foreach ($payments as $payment) {
            $cuopon = $this->Coupon_model->get($payment->coupon_id);
            $csvdata[] = array(
                $payment->order_id,
                $payment->product_name,
                $payment->product_type,
                $payment->amount,
                $payment->amount_paid,
                $payment->quantity,
                $payment->token_no,
                $payment->token_desc,
                $payment->token_amount,
                $payment->payment_type,
                $payment->status,
                $cuopon ? $cuopon->code : '',
                $payment->date_created
            );
        }
//var_dump($orders);
//exit;

        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"user-report-export-" . date('Y-m-d') . ".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");

        $handle = fopen('php://output', 'w');
//fputcsv($handle, array('id','order id',''));
        foreach ($csvdata as $order) {
            fputcsv($handle, $order);
        }
        fclose($handle);
        exit;
    }
}

?>