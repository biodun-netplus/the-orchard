<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('order_model');
        $this->load->library('URLShorterner');
        if (!$this->aauth->is_loggedin())
            redirect('login', 'refresh');


    }


    public function order_new()
    {
        if ($this->aauth->is_member('Admin'))
            redirect('order/list', 'refresh');
        $data = array(
            'title' => 'Order - Create'
        );

        $this->template->load('default', 'account/order/create', $data);
    }


    public function review($order_id)
    {
        $order = $this->order_model->get_by_order_id($order_id);
        if ($order) {

            if ($this->aauth->is_member('Merchant')) {
                $c_user = $this->aauth->get_user();
                if ($order->merchant_id != $c_user->id) {
                    $this->session->set_flashdata('errors', 'Sorry, invalid order selected.');
                    return redirect('order/list', 'refresh');
                }
            }
            $data = array(
                'title' => 'Order - Edit',
                'order' => $order,
                'order_items' => json_decode($order->order_items)
            );

            return $this->template->load('default', 'account/order/review', $data);
        }

        return show_404();
    }


    public function update_customer($order_id)
    {
        if ($order_id) {
            if ($this->aauth->is_member('Admin'))
                redirect('order/list', 'refresh');
            $c_user = $this->aauth->get_user();
            $order_details = $this->order_model->get_by_order_id($order_id);
            if (($order_details) && ($order_details->merchant_id === $c_user->id)) {
                $this->form_validation->set_rules('first_name', 'First Name', 'required');
                $this->form_validation->set_rules('last_name', 'Last Name', 'required');
                $this->form_validation->set_rules('phone', 'Phone', 'required');
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email');


                if ($this->form_validation->run() == TRUE) {
                    $data = array(
                        'contact_email' => $this->input->post('email'),
                        'contact_phone' => $this->input->post('phone'),
                        'customer_firstname' => $this->input->post('first_name'),
                        'customer_lastname' => $this->input->post('last_name'),

                    );


                    if ($result = $this->order_model->update_order($data, $order_details->id)) {
                        $this->session->set_flashdata('success', 'Order successfully update.');
                        return redirect('order/review/' . $order_details->order_id, 'refresh');
                    }
                }

                $errors = (validation_errors() ? validation_errors() : ($this->aauth->get_errors() ? $this->aauth->get_errors() : $this->session->flashdata('errors')));

                $this->session->set_flashdata('errors', $errors);
                return redirect('order/review/' . $order_details->order_id, 'refresh');

            } else {
                $this->session->set_flashdata('errors', 'Invalid order id.');
            }

        } else {
            $this->session->set_flashdata('errors', 'Order id is missing.');
        }

        return redirect('order/list', 'refresh');


    }

    public function update_delivery($order_id)
    {
        if ($order_id) {
            if ($this->aauth->is_member('Admin'))
                redirect('order/list', 'refresh');
            $c_user = $this->aauth->get_user();
            $order_details = $this->order_model->get_by_order_id($order_id);
            if (($order_details) && ($order_details->merchant_id === $c_user->id)) {
                $this->form_validation->set_rules('delivery_street', 'Delivery Street', 'required');
                $this->form_validation->set_rules('delivery_city', 'Delivery City', 'required');
                $this->form_validation->set_rules('delivery_state', 'Delivery State', 'required');


                if ($this->form_validation->run() == TRUE) {
                    $data = array(
                        'delivery_street' => $this->input->post('delivery_street'),
                        'delivery_city' => $this->input->post('delivery_city'),
                        'delivery_state' => $this->input->post('delivery_state'),
                    );


                    if ($result = $this->order_model->update_order($data, $order_details->id)) {
                        $this->session->set_flashdata('success', 'Order successfully update.');
                        return redirect('order/review/' . $order_details->order_id, 'refresh');
                    }
                }

                $errors = (validation_errors() ? validation_errors() : ($this->aauth->get_errors() ? $this->aauth->get_errors() : $this->session->flashdata('errors')));

                $this->session->set_flashdata('errors', $errors);
                return redirect('order/review/' . $order_details->order_id, 'refresh');

            } else {
                $this->session->set_flashdata('errors', 'Invalid order id.');
            }

        } else {
            $this->session->set_flashdata('errors', 'Order id is missing.');
        }

        return redirect('order/list', 'refresh');


    }

    public function cancel($id)
    {
        $order = $this->order_model->get($id);
        if ($order) {

            if ($this->aauth->is_member('Merchant')) {
                $c_user = $this->aauth->get_user();
                if ($order->merchant_id != $c_user->id) {
                    $this->session->set_flashdata('errors', 'Sorry, invalid order selected.');
                    return redirect('order/list', 'refresh');
                }
                if ($order->status === 'Completed') {
                    $this->session->set_flashdata('errors', 'Sorry, order cannot be cancelled only deleted.');
                    return redirect('order/list', 'refresh');
                }
            }


            if ($this->order_model->update_order(array('status' => 'Cancelled'), $id)) {
                $this->session->set_flashdata('success', 'Order cancelled successfully.');
                return redirect('order/list', 'refresh');
            }
        }


        $this->session->set_flashdata('errors', 'An error occured, please try again.');
        return redirect('order/list', 'refresh');
    }

    public function complete($id)
    {
        $order = $this->order_model->get($id);
        if ($order) {

            if ($this->aauth->is_member('Merchant')) {
                $c_user = $this->aauth->get_user();
                if ($order->merchant_id != $c_user->id) {
                    $this->session->set_flashdata('errors', 'Sorry, invalid order selected.');
                    return redirect('order/list', 'refresh');
                }
                if ($order->status === 'Completed') {
                    $this->session->set_flashdata('errors', 'Sorry, order cannot be completed only deleted.');
                    return redirect('order/list', 'refresh');
                }

            }


            if ($this->order_model->update_order(array('status' => 'Completed'), $id)) {
                $this->session->set_flashdata('success', 'Order completed successfully.');
                return redirect('order/list', 'refresh');
            }
        }


        $this->session->set_flashdata('errors', 'An error occured, please try again.');
        return redirect('order/list', 'refresh');
    }

    public function order_create()
    {

        if ($this->aauth->is_member('Admin'))
            redirect('order/list', 'refresh');

        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('order_item[]', 'Order Item', 'required');
        $this->form_validation->set_rules('item_price[]', 'Item Price', 'required');

        $amount = 0;

        $this->form_validation->set_rules('is_delivery', 'Is Delivery', 'required');

        if (!empty($this->input->post('is_delivery', true))) {
            $this->form_validation->set_rules('delivery_street', 'Delivery Street', 'required');
            $this->form_validation->set_rules('delivery_city', 'Delivery City', 'required');
            $this->form_validation->set_rules('delivery_state', 'Delivery State', 'required');
            $this->form_validation->set_rules('delivery_fee', 'Delivery Fee', 'required|numeric');

        }

        if ($this->form_validation->run() == TRUE) {
            $c_user = $this->aauth->get_user();
            $order_items = $this->input->post('order_item');
            $item_price = $this->input->post('item_price');
            $items = array();

            foreach ($order_items as $index => $item) {
                $items[] = array($item => $item_price[$index]);
                $amount += $item_price[$index];
            }
            if (!empty($this->input->post('is_delivery', true))) {
                $items[] = array('Delivery' => $this->input->post('delivery_fee', true));
                $amount += $this->input->post('delivery_fee', true);
            }
            $order_id = 'PLNK' . time();
            $data = array(
                'merchant_id' => $c_user->id,
                'order_id' => $order_id,
                'order_items' => json_encode($items),
                'amount' => $amount,
                'contact_email' => $this->input->post('email'),
                'contact_phone' => $this->input->post('phone'),
                'customer_firstname' => $this->input->post('first_name'),
                'customer_lastname' => $this->input->post('last_name'),
                'currency' => 'NGN',
                'is_delivery' => $this->input->post('is_delivery', true),
                'status' => 'Pending',
                'delivery_street' => ($this->input->post('delivery_street', true)) ? $this->input->post('delivery_street', true) : '',
                'delivery_city' => ($this->input->post('delivery_city', true)) ? $this->input->post('delivery_city', true) : '',
                'delivery_state' => ($this->input->post('delivery_state', true)) ? $this->input->post('delivery_state', true) : '',
            );


            if ($result = $this->order_model->save($data)) {
                $this->session->set_flashdata('success', 'Order created successfully.');

                //create url;
                $unshorten_url = site_url('payment/process/' . $result);

                $shorterner = $this->urlshorterner->shorten($unshorten_url);
                $shorten_url = (is_array($shorterner)) ? $shorterner : json_decode($shorterner);

                if (array_key_exists("id", $shorten_url)) {
                    $url = $shorten_url['id'];
                } else {
                    $url = $unshorten_url;
                }
                /*
                $config = array(
                    'mailtype' => 'html',
                    'protocol' => 'mail',
                    'newline' => "\r\n"
                );
                */
                $config = array(
                    'mailtype' => 'html',
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.gmail.com',
                    'smtp_user' => 'saddle@netplusadvisory.com',
                    'smtp_pass' => 'Saddle7890',
                    'smtp_port' => 465,
                    'priority' => 1,
                    'newline' => "\r\n"
                );



                $merchant = $this->aauth->get_merchant();

                $data_array = array(
                    'url' => $url,
                    'customer_name' => $this->input->post('first_name'),
                    'merchant_phone' => $merchant->phone,
                    'company_name' => $merchant->company_name,
                    'order_items' => $items,
                );

                $message = $this->load->view('email/order.tpl.php', $data_array, true);

                $this->load->library('email', $config);
                $this->email->initialize($config);
                $this->email->set_newline("\r\n");
                $this->email->from('sodiq@hotelnownow.com', 'Orders');
                $this->email->to($this->input->post('email'));

                $this->email->subject('Your Order - ' . $order_id);
                $this->email->message($message);

                $r = $this->email->send();

                if ($r)
                    $this->session->set_flashdata('success', 'Order created and payment email sent successfully.');
                redirect('order/list', 'refresh');

                //var_dump($this->email->print_debugger());
                //var_dump($items);
                //exit;
                /*
                $this->load->view('response', $data);
                exit;
                */
            }
        }


        $errors = (validation_errors() ? validation_errors() : ($this->aauth->get_errors() ? $this->aauth->get_errors() : $this->session->flashdata('errors')));

        $this->session->set_flashdata('errors', $errors);
        redirect('order/new', 'refresh');


    }

    public function delete($id)
    {
        $order = $this->order_model->get($id);
        if ($order) {

            if ($this->aauth->is_member('Merchant')) {
                $c_user = $this->aauth->get_user();
                if ($order->merchant_id != $c_user->id) {
                    $this->session->set_flashdata('errors', 'Sorry, invalid order selected.');
                    return redirect('order/list', 'refresh');
                }
            }

            $this->db->where('id', $id);

            if ($this->db->delete('merchant_orders')) {
                $this->session->set_flashdata('success', 'Order deleted successfully.');
                return redirect('order/list', 'refresh');
            }
        }


        $this->session->set_flashdata('errors', 'An error occured, please try again.');
        return redirect('order/list', 'refresh');
    }


    public function order_list()
    {

        $data['filter_query'] = ($this->input->get('filter_query') ? $this->input->get('filter_query') : null);
        $data['filter_date'] = ($this->input->get('filter_date') ? $this->input->get('filter_date') : null);
        $data['filter_status'] = ($this->input->get('filter_status') ? $this->input->get('filter_status') : null);

        if (!empty($data['filter_date'])) {
            $date_filter = explode(' - ', $data['filter_date']);
            $data['from'] = $date_filter[0];
            $data['to'] = $date_filter[1];
        } else {
            $data['from'] = date('m/d/Y');
            $data['to'] = date('m/d/Y');

        }

        $total_row = count($this->order_model->all_orders(false, false, $data));


        $config = array();
        $config["base_url"] = site_url("order/list");

        $config["total_rows"] = $total_row;
        $config["per_page"] = 10;
        $config["num_tag_open"] = '<li>';
        $config["num_tag_close"] = '</li>';
        $config["prev_tag_open"] = '<li>';
        $config["prev_tag_close"] = '</li>';
        $config["next_tag_open"] = '<li>';
        $config["next_tag_close"] = '</li>';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = $total_row;
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';

        $this->pagination->initialize($config);
        if ($this->uri->segment(3)) {
            $page = ($this->uri->segment(3));
            $offset = ($config["per_page"] * ($page - 1));
        } else {
            $page = 1;
            $offset = ($page - 1);
        }

        $data["orders"] = $this->order_model->all_orders($config["per_page"], $offset, $data);
        $str_links = $this->pagination->create_links();
        $data["links"] = explode('&nbsp;', $str_links);

        $data['title'] = 'Orders - Manage';

        $this->template->load('default', 'account/order/index', $data);

    }


    public function order_report()
    {

        $data['filter_query'] = ($this->input->get('filter_query') ? $this->input->get('filter_query') : null);
        $data['filter_date'] = ($this->input->get('filter_date') ? $this->input->get('filter_date') : null);
        $data['filter_status'] = ($this->input->get('filter_status') ? $this->input->get('filter_status') : null);

        $data['filter_merchant'] = ($this->input->get('filter_merchant') ? $this->input->get('filter_merchant') : null);

        $data['merchants'] = $this->order_model->all_order_merchants();
        if (!empty($data['filter_date'])) {
            $date_filter = explode(' - ', $data['filter_date']);
            $data['from'] = $date_filter[0];
            $data['to'] = $date_filter[1];
        } else {
            $data['from'] = date('m/d/Y');
            $data['to'] = date('m/d/Y');

        }
        $total_row = count($this->order_model->all_orders(false, false, $data));


        $config = array();
        $config["base_url"] = site_url("report/order");

        $config["total_rows"] = $total_row;
        $config["per_page"] = 10;
        $config["num_tag_open"] = '<li>';
        $config["num_tag_close"] = '</li>';
        $config["prev_tag_open"] = '<li>';
        $config["prev_tag_close"] = '</li>';
        $config["next_tag_open"] = '<li>';
        $config["next_tag_close"] = '</li>';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = $total_row;
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';

        $this->pagination->initialize($config);
        if ($this->uri->segment(3)) {
            $page = ($this->uri->segment(3));
            $offset = ($config["per_page"] * ($page - 1));
        } else {
            $page = 1;
            $offset = ($page - 1);
        }

        $data["orders"] = $this->order_model->all_orders($config["per_page"], $offset, $data);
        $str_links = $this->pagination->create_links();
        $data["links"] = explode('&nbsp;', $str_links);
        $data["per_page"] = $config["per_page"];
        $data["total_rows"] = $total_row;
        $data['title'] = 'Orders - Manage';

        $this->template->load('default', 'account/report/order', $data);

    }

    public function delivery_form()
    {
        $data = array(
            'street' => $this->input->get('street'),
            'city' => $this->input->get('city'),
            'state' => $this->input->get('state'),
        );
        return $this->load->view('account/order/form/delivery', $data);
    }


    public function order_export()
    {

        $data['filter_query'] = ($this->input->get('filter_query') ? $this->input->get('filter_query') : null);
        $data['filter_date'] = ($this->input->get('filter_date') ? $this->input->get('filter_date') : null);
        $data['filter_status'] = ($this->input->get('filter_status') ? $this->input->get('filter_status') : null);

        $data['filter_merchant'] = ($this->input->get('filter_merchant') ? $this->input->get('filter_merchant') : null);

        $data['merchants'] = $this->order_model->all_order_merchants();
        if(!empty($data['filter_date'])){
            $date_filter = explode(' - ',$data['filter_date']);
            $data['from'] = $date_filter[0];
            $data['to'] = $date_filter[1];
        }else{
            $data['from'] = date('m/d/Y');
            $data['to'] = date('m/d/Y');

        }
        $orders = $this->order_model->all_orders('','',$data);
        if ($this->aauth->is_member('Admin')) {
            $csvdata[] = array(
                'Order ID',
                'Merchant',
                'Firstname',
                'Lastname',
                'Email',
                'Phone',
                'Items',
                'Total',
                'Transaction ID',
                'Amount Paid',
                'Delivery Street',
                'Delivery City',
                'Delivery State'
            );
        }else{
            $csvdata[] = array(
                'Order ID',
                'Firstname',
                'Lastname',
                'Email',
                'Phone',
                'Items',
                'Total',
                'Transaction ID',
                'Amount Paid',
                'Delivery Street',
                'Delivery City',
                'Delivery State'
            );
        }
        foreach ($orders as $order) {

            $items = '';
            $items_array = json_decode($order->order_items);
            foreach ($items_array as $item) {
                foreach ($item as $product => $price) {
                    $items .= $product . '=' . $price . ' | ';
                }
            }
            if ($this->aauth->is_member('Admin')){

                $csvdata[] = array(
                    $order->order_id,
                    $order->company_name,
                    $order->customer_firstname,
                    $order->customer_lastname,
                    $order->contact_email,
                    $order->contact_phone,
                    $items,
                    $order->amount,
                    $order->transaction_id,
                    $order->amount_paid,
                    $order->delivery_street,
                    $order->delivery_city,
                    $order->delivery_state
                );
        }else{
                $csvdata[] = array(
                    $order->order_id,
                    $order->customer_firstname,
                    $order->customer_lastname,
                    $order->contact_email,
                    $order->contact_phone,
                    $items,
                    $order->amount,
                    $order->transaction_id,
                    $order->amount_paid,
                    $order->delivery_street,
                    $order->delivery_city,
                    $order->delivery_state
                );
            }
        }
        //var_dump($orders);
        //exit;

        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"order-export-".date('Y-m-d').".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");

        $handle = fopen('php://output', 'w');
        //fputcsv($handle, array('id','order id',''));
        foreach ($csvdata as $order) {
            fputcsv($handle, $order);
        }
        fclose($handle);
        exit;
    }


}