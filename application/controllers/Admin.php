<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
	public function __construct() {
        parent::__construct();
        
        $this->load->model('merchant_model');
        if (!$this->aauth->is_loggedin())
            return redirect('login', 'refresh'); 
        if ($this->aauth->is_member('Merchant')){
            show_error('Access Denied');
        }
        
    }
    
    public function users(){
        
        $config = array();
        $config["base_url"] = site_url("admin/users");
        $total_row = count($this->aauth->list_users('Admin','','',true));
        $config["total_rows"] = $total_row;
        $config["per_page"] = 10;
        $config["num_tag_open"] = '<li>';
        $config["num_tag_close"] = '</li>';
        $config["prev_tag_open"] = '<li>';
        $config["prev_tag_close"] = '</li>';
         $config["next_tag_open"] = '<li>';
        $config["next_tag_close"] = '</li>';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = $total_row;
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';

        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
            $offset = ($config["per_page"] * ($page - 1));
        }
        else{
            $page = 1;
             $offset = ($page - 1);
        }
       
        $data["users"] = $this->aauth->list_users('Admin',$config["per_page"],$offset,true);
        $str_links = $this->pagination->create_links();
        $data["links"] = explode('&nbsp;',$str_links );

        $data['title'] = 'Administrator - Users';
 
        $this->template->load('default','account/admin/users/index',$data);
        
    }
    
    public function user_new(){
        
        $data = array(
            'title' => 'Administrator - Create User'
        );
 
        $this->template->load('default','account/admin/users/create',$data);
        
    }
    
    
    public function user_create(){
        
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Password', 'required|matches[password]');
        
        
        if ($this->form_validation->run() == TRUE)
        {
            $data = array(
			'username' => $this->input->post('username',true),
			'email' => $this->input->post('email',true),
			'password' => $this->input->post('password',true)
			);
            if($userid = $this->aauth->create_user($data['email'], $data['password'],$data['username'])){
                if($this->aauth->add_member($userid,'Admin')){
                    $this->session->set_flashdata('success', 'New administrator created.');
                    return redirect('admin/users', 'refresh');
                }
            }
        }
        
        $errors = (validation_errors() ? validation_errors() : ($this->aauth->get_errors() ? $this->aauth->get_errors() : $this->session->flashdata('errors'))); 
        
        $this->session->set_flashdata('errors', $errors);
        return redirect('admin/user/new', 'refresh');
    }
    
     
    public function user_edit($id){
       
        $user = $this->aauth->get_user($id);
        $c_user = $this->aauth->get_user();
       
        if($user->id === $c_user->id){
           
            return redirect('dashboard/profile', 'refresh');
        }
        
        if(!$user){
            $this->session->set_flashdata('errors', 'No user found.');
            return redirect('admin/users', 'refresh');
            
        }
        $data = array(
            'title' => 'Administrator - Edit User',
            'user'  => $user
        );
 
        $this->template->load('default','account/admin/users/edit',$data);
        
    }
    
    public function user_update($id){
        
        $user = $this->aauth->get_user($id);
        $c_user = $this->aauth->get_user();
       
        if($user->id === $c_user->id){
           
            return redirect('dashboard/profile', 'refresh');
        }
        
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('status', 'Account Status', 'required');
        $userid = $user->id;
        
        if ($this->form_validation->run() == TRUE)
        {
          
            if(!empty($this->input->post('password',true))){
                $pass =  $this->input->post('password',true); 
            }else{
                $pass = FALSE;
            }
            
            if($user->username == $this->input->post('username',true)){
                $username =  FALSE;
            }else{
                $username =  $this->input->post('username',true);
            }
            
            $status = $this->input->post('status');
            
            if($status === '0'){
                
                if($this->aauth->unban_user($userid)){
                    $this->session->set_flashdata('success', 'Administrator account updated successfully.');
                }
                
            }elseif($status === '1'){
                if($this->aauth->ban_user($userid)){
                    $this->session->set_flashdata('success', 'Administrator account updated successfully.');
                }
            }
             
            if($this->aauth->update_user($userid,'', $pass, $username)){
                $this->session->set_flashdata('success', 'Administrator account updated successfully.');
                
            }
            
            return redirect('admin/user/edit/'.$userid, 'refresh');
        }
        
        $errors = (validation_errors() ? validation_errors() : ($this->aauth->get_errors() ? $this->aauth->get_errors() : $this->session->flashdata('errors'))); 
       
        $this->session->set_flashdata('errors', $errors);
        return redirect('admin/user/edit/'.$userid, 'refresh');
    }
    
    
    public function user_delete($user_id){
        
        if($this->aauth->delete_user($user_id)){
            $this->session->set_flashdata('success', 'Administrator account deleted successfully.');
            return redirect('admin/users', 'refresh');
        }
        
        $this->session->set_flashdata('errors', 'An error occured, please try again.');
        return redirect('admin/users', 'refresh');
    }
    
    public function merchants(){
              
        $config = array();
        $config["base_url"] = site_url("admin/merchants");
        $total_row = count($this->merchant_model->get_all());
        $config["total_rows"] = $total_row;
        $config["per_page"] = 10;
        $config["num_tag_open"] = '<li>';
        $config["num_tag_close"] = '</li>';
        $config["prev_tag_open"] = '<li>';
        $config["prev_tag_close"] = '</li>';
         $config["next_tag_open"] = '<li>';
        $config["next_tag_close"] = '</li>';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = $total_row;
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';

        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
            $offset = ($config["per_page"] * ($page - 1));
        }
        else{
            $page = 1;
             $offset = ($page - 1);
        }
       
        $data["merchants"] = $this->merchant_model->get_all($config["per_page"],$offset);
        $str_links = $this->pagination->create_links();
        $data["links"] = explode('&nbsp;',$str_links );
        
        $data['title'] = 'Administrator - Merchants';
 
        $this->template->load('default','account/admin/merchants/index',$data);
        
    }
    
    public function merchant_new(){
  
        $data = array(
            'title' => 'Administrator - Create Merchant'
        );
 
        $this->template->load('default','account/admin/merchants/create',$data);
        
    }
    
    
    public function merchant_create(){
        
        $this->form_validation->set_rules('company_name', 'Company Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('contact_no', 'Contact No', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('merchant_id', 'Net Plus Merchant ID', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Password', 'required|matches[password]');
        
        
        $this->form_validation->set_rules('saddle_int', 'Saddle Integration', 'required');
        if(!empty($this->input->post('saddle_int',true))){
            $this->form_validation->set_rules('saddle_client_id', 'Saddle Client ID', 'required');
            $this->form_validation->set_rules('saddle_client_secret', 'Saddle Client Secret', 'required');
            $this->form_validation->set_rules('saddle_pickup_address', 'Saddle Pickup Address', 'required');
            $this->form_validation->set_rules('saddle_pickup_location', 'Saddle Pickup Location', 'required');
            $this->form_validation->set_rules('saddle_pickup_contact_name', 'Saddle Pickup Contact Name', 'required');
            $this->form_validation->set_rules('saddle_pickup_contact_phone', 'Saddle Pickup Contact Phone', 'required');
            $this->form_validation->set_rules('saddle_pickup_contact_email', 'Saddle Pickup Contact Email', 'required');
            $this->form_validation->set_rules('saddle_default_delivery_amount', 'Saddle Default Delivery Amount', 'required');
        
        }
            
            
        
        
        if ($this->form_validation->run() == TRUE)
        {
            
            $data = array(
			'username' => $this->input->post('username',true),
			'email' => $this->input->post('email',true),
			'password' => $this->input->post('password',true)
			);
            if($userid = $this->aauth->create_user($data['email'], $data['password'])){
                if($this->aauth->add_member($userid,'Merchant')){
                    $data = array(
                            'merchant_id' => $userid ,
                            'company_name' => $this->input->post('company_name',true),
                            'phone' => $this->input->post('contact_no',true),
                            'contact_address' => $this->input->post('address',true),
                            'net_plus_merchant_id' => $this->input->post('merchant_id',true),
                            'saddle_int' => $this->input->post('saddle_int',true),
                            'saddle_client_id' => $this->input->post('saddle_client_id',true),
                            'saddle_client_secret' => $this->input->post('saddle_client_secret',true),
                            'saddle_pickup_address' => $this->input->post('saddle_pickup_address',true),
                            'saddle_pickup_location' => $this->input->post('saddle_pickup_location',true),
                            'saddle_pickup_contact_name' => $this->input->post('saddle_pickup_contact_name',true),
                            'saddle_pickup_contact_phone' => $this->input->post('saddle_pickup_contact_phone',true),
                            'saddle_pickup_contact_email' => $this->input->post('saddle_pickup_contact_email',true),
                            'default_delivery_amount' => $this->input->post('saddle_default_delivery_amount',true),
                            );
		            if($this->merchant_model->save($data)){
                        $this->session->set_flashdata('success', 'New Merchant created.');
                        return redirect('admin/merchants', 'refresh'); 
                    }
                    
                    $this->session->set_flashdata('errors', 'Sorry, an error occured, please try again.');
                    
                }
            }
        }
        
        $errors = (validation_errors() ? validation_errors() : ($this->aauth->get_errors() ? $this->aauth->get_errors() : $this->session->flashdata('errors'))); 
        
        $this->session->set_flashdata('errors', $errors);
        return redirect('admin/merchant/new', 'refresh');
    }
    
    public function merchant_edit($id){
       
        $user = $this->aauth->get_user($id);
        $c_user = $this->aauth->get_user();
       
        if($user->id === $c_user->id){
           
            return redirect('dashboard/profile', 'refresh');
        }
        
        if(!$user && $this->aauth->is_member('Merchant',$user->id)){
            $this->session->set_flashdata('errors', 'No merchant found.');
            return redirect('admin/merchants', 'refresh');
            
        }
        
        
        $merchant = $this->merchant_model->get($id);
        $data = array(
            'title' => 'Administrator - Edit Merchant',
            'merchant'  => $merchant
        );
 
        $this->template->load('default','account/admin/merchants/edit',$data);
        
    }
    
    public function merchant_update($id){
        
        $merchant = $this->aauth->get_user($id);
        $c_user = $this->aauth->get_user();
       
        if($merchant->id === $c_user->id){
           
            return redirect('dashboard/profile', 'refresh');
        }
        
        if(!$merchant && $this->aauth->is_member('Merchant',$merchant->id)){
            $this->session->set_flashdata('errors', 'No merchant found.');
            return redirect('admin/merchants', 'refresh');
            
        }
        
        $this->form_validation->set_rules('company_name', 'Company Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('contact_no', 'Contact No', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('merchant_id', 'Net Plus Merchant ID', 'required');
        //$this->form_validation->set_rules('password', 'Password', 'required');
        
        $userid = $merchant->id;

        $this->form_validation->set_rules('saddle_int', 'Saddle Integration', 'required');
        if(!empty($this->input->post('saddle_int',true))){
            $this->form_validation->set_rules('saddle_client_id', 'Saddle Client ID', 'required');
            $this->form_validation->set_rules('saddle_client_secret', 'Saddle Client Secret', 'required');
           $this->form_validation->set_rules('saddle_pickup_address', 'Saddle Pickup Address', 'required');
            $this->form_validation->set_rules('saddle_pickup_location', 'Saddle Pickup Location', 'required');
           $this->form_validation->set_rules('saddle_pickup_contact_name', 'Saddle Pickup Contact Name', 'required');
            $this->form_validation->set_rules('saddle_pickup_contact_phone', 'Saddle Pickup Contact Phone', 'required');
           $this->form_validation->set_rules('saddle_pickup_contact_email', 'Saddle Pickup Contact Email', 'required');
            $this->form_validation->set_rules('saddle_default_delivery_amount', 'Saddle Default Delivery Amount', 'required');

        }
        
        if ($this->form_validation->run() == TRUE)
        {
          
            if(!empty($this->input->post('password',true))){
                $pass =  $this->input->post('password',true); 
            }else{
                $pass = FALSE;
            }
            
            if($merchant->email == $this->input->post('email',true)){
                $email =  FALSE;
            }else{
                $email =  $this->input->post('email',true);
            }
            
            $status = $this->input->post('status');
            
            if($status === '0'){
                
                if($this->aauth->unban_user($userid)){
                    $this->session->set_flashdata('success', 'Merchant account updated successfully.');
                }
                
            }elseif($status === '1'){
                if($this->aauth->ban_user($userid)){
                    $this->session->set_flashdata('success', 'Merchant account updated successfully.');
                }
            }
             
            if($this->aauth->update_user($userid,$email, $pass)){
                $this->session->set_flashdata('success', 'Merchant account updated successfully.');
                
            }
            
            
            $data = array(
                'company_name' => $this->input->post('company_name',true),
                'phone' => $this->input->post('contact_no',true),
                'contact_address' => $this->input->post('address',true),
                'net_plus_merchant_id' => $this->input->post('merchant_id',true),
                'saddle_int' => $this->input->post('saddle_int',true),
                'saddle_client_id' => $this->input->post('saddle_client_id',true),
                'saddle_client_secret' => $this->input->post('saddle_client_secret',true),
                'saddle_pickup_address' => $this->input->post('saddle_pickup_address',true),
                'saddle_pickup_location' => $this->input->post('saddle_pickup_location',true),
                'saddle_pickup_contact_name' => $this->input->post('saddle_pickup_contact_name',true),
                'saddle_pickup_contact_phone' => $this->input->post('saddle_pickup_contact_phone',true),
                'saddle_pickup_contact_email' => $this->input->post('saddle_pickup_contact_email',true),
                'default_delivery_amount' => $this->input->post('saddle_default_delivery_amount',true),
                );
            if($result = $this->merchant_model->update($data,$userid)){
                $this->session->set_flashdata('success', 'Merchant account updated successfully.');
            }
            
            return redirect('admin/merchant/edit/'.$userid, 'refresh');
        }
        
        $errors = (validation_errors() ? validation_errors() : ($this->aauth->get_errors() ? $this->aauth->get_errors() : $this->session->flashdata('errors'))); 
       
        $this->session->set_flashdata('errors', $errors);
        return redirect('admin/merchant/edit/'.$userid);
    }
    
    
    public function merchant_delete($user_id){
        
        if($this->aauth->delete_user($user_id)){
            $this->session->set_flashdata('success', 'Merchant account deleted successfully.');
            return redirect('admin/merchants', 'refresh');
        }
        
        $this->session->set_flashdata('errors', 'An error occured, please try again.');
        return redirect('admin/merchants', 'refresh');
    }
    
    
    public function saddle_form(){
        $merchant = $this->merchant_model->get($this->input->get('merchant_id',true));
        if(is_object($merchant)){
            $data = array(
                'saddle_client_id'=> $merchant->saddle_client_id,
                'saddle_client_secret'=> $merchant->saddle_client_secret,
                'saddle_pickup_address'=>$merchant->saddle_pickup_address,
                'saddle_pickup_location'=>$merchant->saddle_pickup_location,
                'saddle_pickup_contact_name'=>$merchant->saddle_pickup_contact_name,
                'saddle_pickup_contact_phone'=>$merchant->saddle_pickup_contact_phone,
                'saddle_pickup_contact_email'=>$merchant->saddle_pickup_contact_email,
                'default_delivery_amount'=>$merchant->default_delivery_amount ,
            );
        }else{
            $data = array(
                'saddle_client_id'=> '',
                'saddle_client_secret'=> '',
                'saddle_pickup_address'=>'',
                'saddle_pickup_location'=>'',
                'saddle_pickup_contact_name'=>'',
                'saddle_pickup_contact_phone'=>'',
                'saddle_pickup_contact_email'=>'',
                'default_delivery_amount'=>'',
            );
        }

        return $this->load->view('account/admin/form/saddle',$data);
    }
    
}