
<div class="static-content">
<div class="page-content">
  <?php if ($this->aauth->is_member('Merchant')):?>
  <span class="pull-right" style="padding: 10px;">Meter No :
  <?= $this->aauth->get_user()->meter_no ?>
  </span>
  <?php endif; ?>
  <div class="page-heading">
    <h1>Coupons</h1>
    <div class="options"> </div>
  </div>
  <div class="container-fluid">
    <?php $this->load->view('includes/notification'); ?>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <a href="<?php echo site_url('coupon/create'); ?>" class="btn btn-info btn-raised">Create Coupon</a>

            <div class="panel panel-default">
              <div class="panel-heading">
                <h2>Coupon List</h2>
                <div class="panel-ctrls"></div>
              </div>
             
              <div class="panel-body">
                                <table id="defaultTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>Coupon Code</th>
                      <th>Product</th>
                        <th>User</th>
                      <th>Coupon Value</th>
                      <th>Total Usage</th>
                      <th>Usage Count</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
						foreach($coupons as $coupon){?>
                    <tr>
                      <td> <?php echo $coupon->code ?></td>
                      <td> <?php echo $coupon->product_name ?></td>
                       <td> <?php echo $coupon->full_name ?></td>
                        <td> <?php echo $coupon->coupon_value; ?></td>
                      <td> <?php echo $coupon->total_usage; ?></td>
                      <td> <?php echo $coupon->usage_count; ?></td>
                      <td> <?php echo ($coupon->status == 0) ? 'Pending' : 'Used' ?></td>
                      <td>
                            <?php if($coupon->status == 0):?>
                                <a href="<?php echo site_url('coupon/delete/'.$coupon->id); ?>">Delete</a>
                            <?php endif ?>
                       </td>
                    </tr>
                    <?php }?>
                  </tbody>
                </table>

              </div>
                        
             
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .container-fluid --> 
  </div>
  <!-- #page-content --> 
</div>


</body>