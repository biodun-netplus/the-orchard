<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coupon extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('Coupon_model');
        $this->load->model('Product_model');
        $this->load->model('User_model');
        if (!$this->aauth->is_loggedin())
            return redirect('login', 'refresh'); 
        if (!$this->aauth->is_member('Admin')){
            show_error('Access Denied');
        }
        
    }

    public function index(){

        $data = array(
            'title' => 'Coupons'
        );

    	$data['coupons'] =  $this->Coupon_model->all();
        $this->template->load('default', 'coupons/index',$data);
    }

    public function create(){
        $data = array(
            'title' => 'Create Coupon'
        );
        $data['products'] = $this->Product_model->getRecord('product');
        $data['users'] = $this->User_model->user_type_record('Public');
        if(isset($_POST['create_cupon'])){            
            $save_data = [
                'code'=>$this->input->post('code',true),
                'user_id'=>$this->input->post('user',true),
                'product_id'=>$this->input->post('product',true),
                'coupon_value'=>$this->input->post('coupon_value',true),
                'total_usage'=>$this->input->post('total_usage',true)
                ];
            if($this->Coupon_model->check($save_data)){
                $coupon = $this->Coupon_model->save($save_data);
                if($coupon)
                {
                    $this->session->set_flashdata('success', 'Coupon code created successfully.');
                    redirect('coupon');exit;
                }
                $this->session->set_flashdata('errors', 'Coupon code could not be created.');
            }else{
                 $this->session->set_flashdata('errors', 'Coupon code already exist.');
            }
        }
        $this->template->load('default', 'coupons/create',$data);
    }

    public function delete($id){
       $coupon = $this->Coupon_model->get($id);
       if(!$coupon){
            $this->session->set_flashdata('errors', 'Coupon does not exist.');
       }
       if($coupon){
           if($coupon->status == 0){
               if($this->Coupon_model->delete($id)){
                   $this->session->set_flashdata('success', 'Coupon code deleted successfully.');
               }
           }else{
               $this->session->set_flashdata('errors', 'Coupon cannot be delete.');
           }
       }
       redirect('coupon');
    }

}
