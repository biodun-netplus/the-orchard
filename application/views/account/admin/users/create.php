
            <div class="main">
                <div class="breadcrumb">
                    <a href="#">Administrator</a>
                    <span class="breadcrumb-devider">/</span>
                    <a href="<?php echo site_url('admin/users') ?>">User</a>
                    <span class="breadcrumb-devider">/</span>
                    <a href="<?php echo site_url('admin/user/new') ?>">Create</a>
                </div>
                <div class="content">
                    <div class="panel">
                        <div class="content-header no-mg-top">
                            <i class="fa fa-newspaper-o"></i>
                            <div class="content-header-title">Create New Administrator</div>
                        </div>
                        
                        <div class="row" >
                            <div class="col-md-8" style="float:none;margin:auto;">
                                 <?php $this->load->view('includes/notification'); ?>
								
                                <div class="content-box">
                                	<div class="content-box-header">
										<div class="box-title">New Administrator</div>
									</div>
                                    <?php
                                        $attributes = array( 'id' => 'form-validate');
                                        echo form_open('admin/user/create', $attributes);
                                    ?>
									    <div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label>User Name</label>
													<input class="form-control" data-error="Please input username" placeholder="Enter Username" required="required" type="text" name="username">
													<div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
											
                                            <div class="col-sm-6">
												<div class="form-group">
													<label>Email address</label>
                                                    <input class="form-control" data-error="Email address is invalid" placeholder="Enter Email" required="required" type="email"  name="email">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
                                                    <label>Password</label>
                                                    <input class="form-control" data-error="Please input user password" id="inputPassword" placeholder="Enter Password" required="required" type="password"  name="password">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
                                            <div class="col-sm-6">
												<div class="form-group">
                                                    <label>Confirm Password</label>
                                                    <input class="form-control" data-error="confirm password is required" placeholder="Confirm Password" required="required" type="password"  name="confirm_password" data-match="#inputPassword" data-match-error="Whoops, password don't match">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
										</div>
                                     
                                        
                                        
										<div class="content-box-footer">
											<button class="btn btn-primary" type="submit"><i class="fa fa-pencil"></i> Create</button>
										</div>
									</form>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>