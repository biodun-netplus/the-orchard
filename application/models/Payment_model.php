<?php

class Payment_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

    }

    public function save($data)
    {
        $this->db->insert('payments', $data);
        $lastId = $this->db->insert_id();
        return $lastId;

    }

    public function get($where)
    {
        $this->db->select('*');
        $query = $this->db->get_where('payments', $where);
        return $query->row();
    }

    public function getAdminPayment()
    {
        $this->db->where('payments.admin_payment', 1);
        $this->db->order_by('payments.date_created','desc');
        $this->db->join('aauth_users', 'payments.user_id = aauth_users.id');
       $this->db->from('payments');
       $query = $this->db->get();  

       return $query->result();
    }

    public function getMonthlyCount(){
        $prev_month_start = strtotime('first day of previous month', time());
        $prev_month_end = strtotime('last day of previous month', time());
        $prevStartDate = date('Y-m-d', $prev_month_start) . ' 00:00:00';
        $prevEndDate = date('Y-m-d', $prev_month_end) . ' 23:59:59';

        $count = $this->db->query("SELECT count(*) as counted FROM `payments` WHERE `admin_payment` = '1' AND `status` = 'Paid' AND
         `date_created` BETWEEN '$prevStartDate' and '$prevEndDate'")->result();
        $admin_count = $count[0]->counted;

        return $admin_count;
    }

    public function getAdminPowerPayment(){
        

        $count = $this->db->query("SELECT count(*) as counted FROM `payments` WHERE `admin_payment` = '1' AND `status` = 'Paid'")->result();
        $admin_count = $count[0]->counted;
        $admin_payment = $admin_count * 100;
       
        return $admin_payment;
    }

    public function getAdminPaymentByUserId($user_id)
    {
        $this->db->where('payments.admin_payment', 1);
        $this->db->where('payments.user_id', $user_id);
        $this->db->where('payments.status', 'Paid');
        $this->db->order_by('payments.date_created','desc');
        $this->db->join('aauth_users', 'payments.user_id = aauth_users.id');
       $this->db->from('payments');
       $query = $this->db->get();  

       return $query->result();
    }

    public function update($data,$id){
        $this->db->where('id', $id);
        if ($this->db->update('payments', $data)) {
            return true;
        }

        return false;
    }

    public function all($limit = FALSE,$offset = FALSE){
        if($limit){
            $this->db->limit($limit);
            if($offset)
                $this->db->limit($limit,$offset);
        }
        $this->db->where('payments.status', 'paid');
        $this->db->order_by('payments.date_created','desc');
		$this->db->join('cart', 'payments.payment_id = cart.order_id');
        $this->db->join('product', ' cart.product_id = product.id');
        $this->db->join('aauth_users', 'payments.user_id = aauth_users.id');
       $this->db->from('payments');
	   $query = $this->db->get();  

        return $query->result();
    }

    public function outstanding($limit = FALSE,$offset = FALSE){
        if($limit){
            $this->db->limit($limit);
            if($offset)
                $this->db->limit($limit,$offset);
        }
        $this->db->order_by('payments.date_created','desc');
        $this->db->join('cart', 'payments.payment_id = cart.order_id');
        $this->db->join('outstanding_bills', 'payments.payment_id = outstanding_bills.order_id');
        //$this->db->join('product', ' cart.product_id = product.id');
        $this->db->join('aauth_users', 'payments.user_id = aauth_users.id');
       $this->db->from('payments');
	   $query = $this->db->get();  
    
        return $query->result();
    }
}

?>